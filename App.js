/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {useDispatch} from 'redux';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {NavigationContainer} from '@react-navigation/native';

const Tab = createMaterialTopTabNavigator();
import {connect} from 'react-redux';
import Screen from './src/MainScreen';

const App: () => React$Node = (props) => {
  useEffect(() => {
    props.getFood();
  }, []);

  const {data} = props;
  console.log(data.size);
  return (
    <>
      <StatusBar barStyle="dark-content" />
      {data.length > 0 && (
        <NavigationContainer>
          <Tab.Navigator>
            {data.map((value) => (
              <Tab.Screen
                name={`${value.name}(${value.items.length})`}
                component={() => <Screen items={value.items} />}
              />
            ))}
          </Tab.Navigator>
        </NavigationContainer>
      )}
      {/* <Screen /> */}
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

const mapStateToProp = (state) => {
  return {
    data: state.foodData.data,
  };
};

const mapDisptachToProp = (dispatch) => {
  return {
    getFood: () => dispatch({type: 'GET_FOOD'}),
  };
};

export default connect(mapStateToProp, mapDisptachToProp)(App);
