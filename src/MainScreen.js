import React, {useEffect} from 'react';
import {View, FlatList, StyleSheet, Text} from 'react-native';

import {connect} from 'react-redux';
import {GET_FOOD} from './store/action/FoodAction';
import ItemFood from './component/ItemFood';
import {useDispatch} from 'react-redux';

const Screen = (props) => {
  return (
    <View style={{flex: 1}}>
      <FlatList
        data={props.items}
        renderItem={({item}) => <ItemFood item={item} />}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
};

const Styles = StyleSheet.create({});

const mapStateToProp = (state) => {
  return {};
};

const mapDisptachToProp = (dispatch) => {
  return {};
};

export default connect(mapStateToProp, mapDisptachToProp)(Screen);
