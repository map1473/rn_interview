import React from 'react';
import {View, StyleSheet, Image, ImageBackground, Text} from 'react-native';

const ItemFood = ({item}) => {
  console.log(item);
  return (
    <View style={Styles.container}>
      <Image source={{uri: item.image}} style={Styles.leftImg} />
      <View style={Styles.rightContainer}>
        <Text>{item.name}</Text>
        <Text>{item.price}</Text>
      </View>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderRadius: 10,
    borderColor: 'green',
    marginVertical: 8,
    marginHorizontal: 24,
  },
  leftImg: {
    width: '30%',
    height: 75,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  rightContainer: {
    flex: 1,
    alignItems: 'center',
  },
});

export default React.memo(ItemFood);
