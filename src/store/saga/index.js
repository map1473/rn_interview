import FoodSaga from './FootSaga';
import {all} from 'redux-saga/effects';

function* rootSaga() {
  yield all([FoodSaga()]);
}

export default rootSaga;
