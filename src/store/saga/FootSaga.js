import {put, takeLatest, all, call} from 'redux-saga/effects';
import {
  GET_FOOD,
  GET_FOOD_SUCCESS,
  GET_FOOD_FAILED,
} from '../action/FoodAction';
import axio from 'axios';

function* callApi(action) {
  try {
    const response = yield call(
      axio.get,
      'http://ant-tech.free.beeceptor.com/data',
    );
    if (response.data) {
      yield put({
        type: GET_FOOD_SUCCESS,
        payload: response.data,
      });
    } else {
      yield put({
        type: GET_FOOD_FAILED,
        payload: 'Something went wrong',
      });
    }
  } catch (e) {
    console.log(JSON.stringify(e));
  }
}

export default function* watchFoodCallApi() {
  yield takeLatest(GET_FOOD, callApi);
}
