import {GET_FOOD_SUCCESS, GET_FOOD_FAILED} from '../action/FoodAction';

const initialState = {
  data: [
    {
      id: '20',
      name: 'Burgers',
      items: [
        {
          id: '2001',
          name: 'Grilled Chicken Burger',
          price: 7,
          image: 'https://images.unsplash.com/photo-1568901346375-23c9450c58cd',
        },
        {
          id: '2002',
          name: 'Mega Burger',
          price: 10,
          image: 'https://images.unsplash.com/photo-1568901346375-23c9450c58cd',
        },
        {
          id: '2003',
          name: 'B.B.Q Burger',
          price: 8,
          image: 'https://images.unsplash.com/photo-1568901346375-23c9450c58cd',
        },
        {
          id: '2004',
          name: 'Spicy Chicken Burger',
          price: 7,
          image: 'https://images.unsplash.com/photo-1568901346375-23c9450c58cd',
        },
        {
          id: '2005',
          name: 'Crispy Burger',
          price: 7,
          image: '',
        },
      ],
    },
    {
      id: '21',
      name: 'Sandwich',
      items: [
        {
          id: '2101',
          name: 'Ultimate Club Sandwich',
          price: 7,
          image:
            'https://monngondongian.com/wp-content/uploads/2019/06/banh-mi-sandwich-cuc-thom-va-mem.png',
        },
        {
          id: '2102',
          name: 'Chicken & Cheese Club Sandwich',
          price: 10,
          image:
            'https://monngondongian.com/wp-content/uploads/2019/06/banh-mi-sandwich-cuc-thom-va-mem.png',
        },
      ],
    },
    {
      id: '22',
      name: 'Pizza',
      items: [
        {
          id: '2201',
          name: 'Italian Pizza',
          price: 9,
          image: '',
        },
        {
          id: '2202',
          name: 'Texas Pizza',
          price: 10,
          image:
            'https://cdn-www.vinid.net/2020/04/4692c9e2-huong-dan-cach-lam-banh-pizza-tai-nha-khong-can-lo-nuong.jpg',
        },
        {
          id: '2203',
          name: 'Cheese Pizza',
          price: 10,
          image:
            'https://cdn-www.vinid.net/2020/04/4692c9e2-huong-dan-cach-lam-banh-pizza-tai-nha-khong-can-lo-nuong.jpg',
        },
      ],
    },
  ],
};

const handleSuccess = (state, data) => {
  console.log(JSON.stringify(data));
  return {
    ...state,
    data: data[0],
  };
};

export default function FoodReducer(state = initialState, action) {
  switch (action.type) {
    case GET_FOOD_SUCCESS:
      return handleSuccess(state, action.payload);
    case GET_FOOD_FAILED:
      return state;
    default:
      return state;
  }
}
