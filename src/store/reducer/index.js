import {combineReducers} from 'redux';
import FoodReducer from './FoodReducer';

const rootReducer = combineReducers({
  foodData: FoodReducer,
});

export default rootReducer;
